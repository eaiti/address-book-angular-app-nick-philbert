import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Contact } from './contact';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
    'Access-Control-Allow-Headers': 'Origin, Content-Type'
  })
};


@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private contactUrl = '/api/contact/';
  private contactsUrl = '/api/list/';
  private searchUrl = '/api/search/';


  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }

  // GET contact list
  getContacts(): Observable<Contact[]> {
    return this.http.get<Contact[]>(this.contactsUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getContacts', []))
      );
  }

  // GET contact
  getContact(id: string): Observable<Contact> {
    const contactUrl = `${this.contactUrl}${id}`;

    return this.http.get<Contact>(contactUrl, httpOptions).pipe(
      catchError(this.handleError<Contact>('getContact'))
    );
  }

  // POST contact
  addContact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(this.contactUrl, contact, httpOptions).pipe(
      tap(_ => this.log(`Added contact ${contact.nameFirst}`)),
      catchError(this.handleError<Contact>('addContact', contact))
    );
  }

  // PUT contact
  updateContact(contact: Contact): Observable<Contact> {
    const contactUrl = `${this.contactUrl}${contact._id}`;
    return this.http.put<Contact>(contactUrl, contact, httpOptions).pipe(
      tap(_ => this.log(`Updated contact ${contact.nameFirst}`)),
      catchError(this.handleError<Contact>('updateContact', contact))
    );
  }

  // DELETE contact
  deleteContact(id: string): Observable<Contact> {
    const url = `${this.contactUrl}${id}`;

    return this.http.delete<Contact>(url, httpOptions).pipe(
      tap(_ => this.log(`Successfully deleted contact`)),
      catchError(this.handleError<Contact>('deleteContact'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: HttpErrorResponse): Observable<T> => {
      this.logError(`${error.error.status}: ${operation}, ${error.error.message}`);

      return of(result);
    };
  }

  private log(message: string) {
    this.messageService.addNotification(message);
  }

  private logError(message: string) {
    this.messageService.addError(message);
  }

  searchContacts(term: string): Observable<Contact[]> {
    const searchUrl = `${this.searchUrl}${term}`;

    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Contact[]>(searchUrl).pipe(
      catchError(this.handleError<Contact[]>('searchContacts', []))
    );
  }
}
