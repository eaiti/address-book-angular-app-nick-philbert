import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  // Observable string sources
  private errorMessagesSource = new Subject<string>();
  private notificationMessagesSource = new Subject<string>();

  // Observable string streams
  errorMessages$ = this.errorMessagesSource.asObservable();
  notificationMessages$ = this.notificationMessagesSource.asObservable();

  addError(message: string) {
    this.errorMessagesSource.next(message);
  }

  addNotification(message: string) {
    this.notificationMessagesSource.next(message);
  }
}
