export interface Contact {
    _id: string;
    nameFirst: string;
    nameLast: string;
    phoneNumber: string;
    address: Address;
}

interface Address {
    street: string;
    city: string;
    zipCode: string;
    state: string;
}
