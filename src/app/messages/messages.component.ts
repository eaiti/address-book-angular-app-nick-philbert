import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from '../message.service';
import { MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages-component-error',
  template: `
    <span class="error">
      Error: {{this.message}}
    </span>
  `,
  styles: [`
    .error {
      color: #f44336;
    }
  `]
})
export class ErrorMessageComponent {
  public message;
}

@Component({
  selector: 'app-messages-component-notification',
  template: `
    <span>
      Notification: {{this.message}}
    </span>
  `
})
export class NotificationMessageComponent {
  public message;
}


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
  // providers: [MessageService]
})
export class MessagesComponent implements OnInit, OnDestroy {
  private errorSubscription: Subscription;
  private notificationSubscription: Subscription;

  constructor(public messageService: MessageService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.errorSubscription = this.messageService.errorMessages$.subscribe(
      errorMessage => {
        this.openSnackBarError(errorMessage);
      }
    );

    this.notificationSubscription = this.messageService.notificationMessages$.subscribe(
      notificationMessage => {
        this.openSnackBarNotification(notificationMessage);
      }
    );
  }

  ngOnDestroy() {
    this.errorSubscription.unsubscribe();
    this.notificationSubscription.unsubscribe();
  }

  openSnackBarError(messageContent: string) {
    const ref = this.snackBar.openFromComponent(ErrorMessageComponent, {
      duration: 5000,
    });
    ref.instance.message = messageContent;
  }

  openSnackBarNotification(messageContent: string) {
    const ref = this.snackBar.openFromComponent(NotificationMessageComponent, {
      duration: 5000,
    });
    ref.instance.message = messageContent;
  }
}
