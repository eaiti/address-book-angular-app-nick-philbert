import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Contact } from '../contact';
import { ContactService } from '../contact.service';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { DeleteConfirmDialogComponent } from '../delete-confirm-dialog/delete-confirm-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  displayedColumns: string[] = ['nameFirst', 'nameLast', 'phoneNumber', 'details', 'delete'];
  dataSource = new MatTableDataSource<Contact>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private contactService: ContactService,
    public scrollDispatcher: ScrollDispatcher,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.getContacts();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    console.log(this.dataSource);
  }

  getContacts(): void {
    this.contactService.getContacts()
      .subscribe(contacts => {
        this.dataSource.data = contacts;
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  toDetails(id: string) {
    // TODO: route to details
    this.router.navigate([`/contact-form/${id}`]);
  }

  onDelete(id: string) {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      // Handle dialog closure
      if (result) {
        this.contactService.deleteContact(id)
          .subscribe(
            () => location.reload()
          );
      }
    });
  }

}

