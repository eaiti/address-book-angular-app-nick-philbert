import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

import { Contact } from '../contact';
import { ContactService } from '../contact.service';
import { FormGroup, FormControl, RequiredValidator, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {
  contact: Partial<Contact> = {};
  submitted = false;
  callType: string;
  contactFormGroup: FormGroup;
  isUpdate: boolean;
  id: string;

  constructor(
    private contactService: ContactService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.contactFormGroup = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      nameFirst: new FormControl('', [Validators.required]),
      nameLast: new FormControl(),
      phoneNumber: new FormControl('', [Validators.required, Validators.pattern(RegExp('^[0-9-()]*$'))]),
      address: new FormGroup({
        street: new FormControl(),
        city: new FormControl(),
        state: new FormControl(),
        zipCode: new FormControl()
      })
    }
    );
  }

  ngOnInit() {
    if (this.route.snapshot.paramMap.has('id')) {
      this.isUpdate = true;
      this.id = this.route.snapshot.paramMap.get('id');
      this.contactService.getContact(this.id)
        .subscribe(contact => {
          this.contact = contact;
          this.contactFormGroup.patchValue(this.contact);
        });
    } else {
      this.isUpdate = false;
      this.contactFormGroup.patchValue(this.contact);
    }
  }

  onSubmit() {
    this.contact = this.contactFormGroup.value;
    this.contact._id = this.id;

    if (this.isUpdate) {
      this.update(this.contact as Contact);
    } else {
      this.add(this.contact as Contact);
    }

  }

  onSuccess() {
    this.router.navigate(['contacts']);
    this.submitted = true;
  }

  add(contact: Contact): void {
    if (!contact) { return; }
    this.contactService.addContact(contact)
      .subscribe(
        c => {
          console.log(c);
          if (!this.isContact(c)) {
            this.onSuccess();
          }
        }
      );
  }

  isContact(object: any): object is Contact {
    return 'nameFirst' in object && 'phoneNumber' in object;
  }

  update(contact: Contact): void {
    if (!contact) { return; }
    this.contactService.updateContact(contact)
      .subscribe(
        c => {
          if (!this.isContact(c)) {
            this.onSuccess();
          }
        }
      );
  }

  goBack(): void {
    this.contactFormGroup.reset();
    this.location.back();
  }

}
